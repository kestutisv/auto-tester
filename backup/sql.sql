CREATE TABLE IF NOT EXISTS `submissions` (
  `ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `task_ID` int(9) unsigned NOT NULL,
  `user_ID` int(9) NOT NULL,
  `code_path` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  -- is compile successful 
  PRIMARY KEY (`ID`),
  KEY `task_ID` (`task_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `submissions_results`
--

CREATE TABLE IF NOT EXISTS `submissions_results` (
  `ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(9) unsigned NOT NULL,
  `test_ID` int(9) unsigned NOT NULL,
  `input` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `output_expected` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `output_generated` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `isPassed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(9) unsigned NOT NULL COMMENT 'Creator',
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  `description` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `input` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `output` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `examples` mediumtext COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Ar yra naudinga daugiau normalizuoti?',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `user_ID` int(9) NOT NULL COMMENT 'creator',
  `input` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  `output` mediumtext COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_lithuanian_ci NOT NULL,
  `password` text COLLATE utf8_lithuanian_ci NOT NULL,
  `permission_level` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=1 ;

