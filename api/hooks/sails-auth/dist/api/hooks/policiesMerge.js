/**
 * Created by Vytautas on 2016-02-23.
 */

'use strict';

var _ = require('lodash');
_.mergeWith = require('lodash.mergewith');

var customAfter = {
  '**': ['passport']
};

var customBefore; // = {};

function mergePolicies(objValue, srcValue, key, object, source, stack) {
  var retVal;
  if (objValue === false || srcValue === false) {
    retVal = false;
  } else if (objValue === true || objValue === undefined) {
    retVal = srcValue;
  } else if (srcValue === true) {
    retVal = objValue;
  } else if (_.isArray(objValue) && _.isArray(srcValue)) {
    retVal = _.union(objValue, srcValue);
  } else if (_.isArray(objValue) || _.isArray(srcValue)) {
    throw('can not concat array with not array');
  } else {
    retVal = _.clone(objValue);
    _.mergeWith(retVal, srcValue, mergePolicies);
  }
  return retVal;
}

function appendAll(obj, policies) {
  if (obj.hasOwnProperty('**')) {
    policies = _.union(policies, obj['**']);
    delete obj['**'];
  }
  Object.keys(obj).forEach(function (key) {
    if (obj[key] === false) {
    } else if (obj[key] === true) {
      obj[key] = policies;
    } else if (_.isArray(obj[key])) {
      obj[key] = _.union(policies, obj[key])
    } else if (_.isObject(obj[key])) {
      appendAll(obj[key], policies);
    }
  });
  return obj;
}

module.exports =  function (sails){
  var policies = sails.config.policies;
  if (_.isObject(customBefore)) {
    _.mergeWith(customBefore, policies, mergePolicies);
  }
  if (_.isObject(customAfter)) {
    _.mergeWith(policies, customAfter, mergePolicies);
  }
  appendAll(policies);
  //sails.log.info(policies);
  return policies;
};
