/**
 * Task.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


function handle(err, res) {
  sails.log.error(err);
  res.serverError(err);
}

module.exports = {

  find: function(req, res) {
    Task.find().populate('createdBy').exec(function(err, data) {
      res.json(data);
    });
  },

 //find: function(req, res) {
 //   Task.find().populate('createdBy').exec(function(err, data) {
 //     res.json(data.map(function(task) {
 //       return {
 //         id: task.id,
 //         title: task.title,
 //         createdBy: {
 //           id: task.createdBy.id,
 //           name: task.createdBy.username
 //         }
 //       }
 //     }));
 //   })
 // }

  create: function(req, res) {
    var tastToSave = {
      createdBy: req.user.id,
      title: req.body.title,
      examples: req.body.examples,
      description: req.body.description,
      input: req.body.input,
      output: req.body.output
    };

    Task.create(tastToSave).exec(function(err, data) {
      if (err) return handle(err, res);

      TestContainer.native(function(err, coll) {
        if (err) return handle(err, res);

        coll.insertOne({
          checker: 'identical',
          tests: req.body.tests,
          task: data.id
        }, function(err, container) {
          if (err) return handle(err, res);

          Task.update(data.id, {tests: container.insertedId.toString()}).exec(function(err, data) {
            if (err) return handle(err, res);

            res.json(data);
          });

        });
      });
    });
  }
};

