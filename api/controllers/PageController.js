'use strict';

module.exports = {
  index: function (req, res) {
    if(req.isAuthenticated())
    {
      res.redirect('/app');
    } else {
      res.redirect('/guest');
    }
  },

  viewGuest: function (req, res) {
    if(req.isAuthenticated())
    {
      res.redirect('/app');
    } else {
      res.view('homepage');
    }
  },

  viewSecret: function (req, res) {
    if(req.isAuthenticated())
    {
      res.view('secret');
    } else {
      res.redirect('/guest');
    }
  },
};
