/**
 * Submission.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var request = require('request');
var _ = require('lodash');


function handle(err, res) {
  sails.log.error(err);
  res.serverError(err);
};

module.exports = {
  create: function(req, res) {
    var dto = req.body;
    var sub = {};
    var valid = true;

    sub.userId = req.user.id;
    sub.taskId =  parseInt(dto.task);
    sub.code = dto.code;
    sub.language = dto.language;
    sub.fileName = dto.language === 'java' ? dto.className:null;

    _.forOwn(sub, function(value, key) { if(value === undefined) valid = false; } );

    if(!valid) {
      res.status(400);
      return res.send("validation error");
    }


    Task.findOne({id: sub.taskId}).exec(function(err, task) {
      if (err) return handle(err, res);

      sub.testsId = task.tests;

      Submission.native(function(err, coll) {
        if (err) return handle(err, res);

        coll.insertOne(sub, function(err, savedSub) {
          if (err) return handle(err, res);

          request('http://52.39.110.44:4567/solution/'+savedSub.insertedId.toString());
          res.json({id: savedSub.insertedId.toString()});
        });
      });

    });
    //Submission.create(submission).then(function(sub) {
    //  res.status(201);
    //  request('http://52.39.110.44:4567/solution/'+sub.id);
    //  res.json({id: sub.id});
    //})


  }
};

