/**
 * Submission.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection: 'remoteMongoDB',
  attributes: {
    task: {
      model: 'task',
      columnName: 'taskId'
    },
    taskTests: {
      model: 'testContainer',
      columnName: 'TestsId'
    },
    user: {
      model: 'user',
      columnName: 'userId'
    },
    code: {
      type: 'text',
      columnName: 'code'
    },
    tests: {
      collection: 'submissionTest',
      via: 'submission'
    }
  }
};

