/**
 * Task.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    createdBy: {
      columnName: 'creator_id',
      model: 'user',
      required: true
    },
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'text',
      required: true
    },
    input: {
      type: 'text',
      required: true
    },
    output: {
      type: 'text',
      required: true
    },
    examples: {
      collection: 'TaskIOExample',
      via: 'taskId'
    },
    tests: {
      model: 'TestContainer'
    }

  }
};

