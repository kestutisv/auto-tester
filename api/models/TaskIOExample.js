/**
 * TaskIOExample.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'task_io_example',
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    input: {
      type: 'string'
    },
    output: {
      type: 'string'
    },
    taskId: {
      //type: 'integer',
      columnName: 'task_id',
      model: 'task'
    }
  }
};

