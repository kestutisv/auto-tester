/**
 * SubmissionTest.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection: 'remoteMongoDB',
  tableName: 'submission_test',
  attributes: {
    generatedOutput: {
      type: 'text',
      columnName: 'generated_output'
    },
    submission: {
      columnName: 'submission_id',
      model: 'submission'
    },
    test: {
      model: 'test',
      columnName: 'test_id'
    }
  }
};

