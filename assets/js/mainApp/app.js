
var mainApp = angular.module('mainApp', ['ngMaterial', 'ui.router']);

mainApp.config(function($stateProvider, $urlRouterProvider, $mdThemingProvider, $locationProvider) {
  $urlRouterProvider.otherwise('/home');

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: '/templates/main/home.html',
      title: 'Welcome'
    })
    .state('newtask', {
      url: '/newtask',
      templateUrl: '/templates/main/ikelti uzduoti.html',
      title: 'Create task',
      controller: 'newTaskCtrl as ntc'
    })
    .state('tasklist', {
      url: '/tasks',
      templateUrl: '/templates/main/tasklist.html',
      title: 'Available tasks',
      controller: 'tasklistCtrl as tlc'
    })
    .state('solutionlist', {
      url: '/solutions',
      templateUrl: '/templates/main/userSolutionList.html',
      title: 'My solutions',
      controller: 'userSolutionListCtrl as uslc'
    })
    .state('usertasklist', {
      url: '/mytasks',
      templateUrl: '/templates/main/userTaskList.html',
      title: 'My tasks',
      controller: 'userTaskListCtrl as utlc'
    })
    .state('task', {
      url: '/tasks/:id',
      templateUrl: '/templates/main/task.html',
      title: 'Solve tasks',
      controller: 'taskCtrl as kc'
    })
    .state('solution', {
      url: '/solutions/:id',
      templateUrl: '/templates/main/usersolution.html',
      title: 'Solution overview',
      controller: 'userSolutionCtrl as usc'
    });

  $mdThemingProvider
    .theme('default')
    .primaryPalette('amber')
    .accentPalette('lime')
    .warnPalette('red')
  $locationProvider.html5Mode(true);
});
