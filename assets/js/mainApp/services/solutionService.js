'use strict';

mainApp.factory('solutionService', function($http) {
  var solutionService = {};

  solutionService.model = {
    tasks: []
  };

  solutionService.send = function(data) {
    return $http.post('/submission', data);
  };

  return solutionService;
});
