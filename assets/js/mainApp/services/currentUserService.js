'use strict';

mainApp.factory('currentUserService', function($http) {
  var currentUserService = {};

  currentUserService.model = {
    user: {}
  };

  currentUserService.get = function() {
    return $http.get('/user/me')
      .then(function(data) {
        currentUserService.model.user = data.data
      })
  };

  return currentUserService;
});
