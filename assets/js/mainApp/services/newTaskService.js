'use strict';

mainApp.factory('newTaskService', function($http) {
  var newTaskService = {};


  newTaskService.create = function(task) {
    console.log(task);

    return $http.post('/task', {
      title: task.taskName,
      description: task.taskMainDescription,
      input: task.taskInputDescription,
      output: task.taskOutputDescription,
      examples: task.examples.map(function(e) {
        return {
          input: e.input,
          output: e.output
        }
      }),
      tests: task.tests.map(function(e) {
        return {
          input: e.input,
          output: e.output
        }
      })
    }).then(function(res) {
      console.log(res);
      return res;
    })
  };

  return newTaskService;
});
