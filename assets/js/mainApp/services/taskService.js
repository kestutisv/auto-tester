'use strict';

mainApp.factory('taskService', function($http) {
  var taskService = {};

  taskService.model = {
    task: {}
  };

  taskService.get = function(id) {
    return $http.get('/task/'+id)
      .then(function(data) {
        taskService.model.task = data.data
      })
  };

  return taskService;
});
