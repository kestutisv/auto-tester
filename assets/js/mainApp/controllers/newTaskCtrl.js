'use strict';

mainApp.controller('newTaskCtrl', function ($state, newTaskService) {
  var vm = this;
  var sending = false;

  vm.form = {
    taskName: '',
    taskMemoryLimit: '0',
    taskTimeLimit: '0',
    taskMemoryLimitState: 'MB',
    taskTimeLimitState: 's',
    taskMainDescription: '',
    taskInputDescription: '',
    taskOutputDescription: '',
    examples: [],
    tests: []
  };

  vm.addExampleEntry = function () {
    vm.form.examples.push({
      id: vm.form.examples.length,
      input: vm.form.taskExampleInput,
      output: vm.form.taskExampleOutput
    });

    vm.form.taskExampleInput = '';
    vm.form.taskExampleOutput = '';
  };

  vm.addTestEntry = function () {
    vm.form.tests.push({
      id: vm.form.tests.length,
      input: vm.form.taskTestInput,
      output: vm.form.taskTestOutput
    });

    vm.form.taskTestInput = '';
    vm.form.taskTestOutput = '';
  };

  vm.totalExamples = function () {
    return vm.form.examples.length;
  };
  vm.totalTests = function () {
    return vm.form.tests.length;
  };


  vm.editEntry = function (edit) {
    vm.form.taskExampleInput = edit.input;
    vm.form.taskExampleOutput = edit.output;
    vm.form.examples.splice(vm.form.examples.indexOf(edit), 1);
  };

  vm.editTestEntry = function (edit) {
    vm.form.taskTestInput = edit.input;
    vm.form.taskTestOutput = edit.output;
    vm.form.tests.splice(vm.form.tests.indexOf(edit), 1);
  };

  vm.removeEntry = function (remove) {
    vm.form.examples.splice(vm.form.examples.indexOf(remove), 1);
  };

  vm.resetForm = function () {
    vm.form.taskName = '';
    vm.form.taskMemoryLimit = '0';
    vm.form.taskTimeLimit = '0';
    vm.form.taskMemoryLimitState = 'MB';
    vm.form.taskTimeLimitState = 's';
    vm.form.taskMainDescription = '';
    vm.form.taskInputDescription = '';
    vm.form.taskOutputDescription = '';
    vm.form.taskExampleInput = '';
    vm.form.taskExampleOutput = '';
    vm.taskTestExamples = [];
  };

  vm.create = function (task) {
    if (!sending) {
      sending = true;
      newTaskService.create(task).then(function (res) {
        $state.go('task', {id: res.data[0].id});
      })
        .catch(function (res) {
          alert("Sorry, something went wrong");
        })
      .finally(function(){
        sending = false;
      });

    }
  }


});
