'use strict';

mainApp.controller('userSolutionCtrl', function($stateParams, $mdDialog) {
  var vm = this;

  vm.taskId = $stateParams.id;

  vm.task = {
    title: 'mock Task Title',
    mainDescription: 'this is mock Task\nThis has no real meaning\nRada Rada rada',
    inputDescription: 'this is mock input description, please replace this with good stuff',
    outputDescription: 'this is mock output description, please replace this with real shit',
    creatorName: 'douchey McDouchins'
  };
  vm.testResults = [
    { input: '1 2', output: '3', result: true },
    { input: '3 2', output: '5', result: true },
    { input: '444444444444 444444444444', output: 'Error', result: false },
    { input: '10 -10', output: '0', result: true }
  ];

  vm.showSolutionUpload = function(ev){
    $mdDialog.show({
      templateUrl: '/templates/main/upload_form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
      .then(function(answer) {
        alert('Išsiunčiamas failas tikrinimui')
      }, function() {
        alert('Uždaromas modalas')
      });
  }
});
