'use strict';

mainApp.controller('userSolutionListCtrl', function() {
  var vm = this;

  vm.solutionlist = [
    {
      id: '010101',
      taskName: 'mockup task Name',
      solutionResults: '9/11'
    },
    {
      id: '10101010',
      taskName: 'mockup task Name',
      solutionResults: '7/54'
    }
  ]
});
