'use strict';

mainApp.controller('taskCtrl', function($stateParams, $mdDialog, taskService) {
  var vm = this;

  vm.taskId =  $stateParams.id;
  vm.model = taskService.model;

  vm.init = function(){
    taskService.get(vm.taskId);
  }
  vm.showMySolutions = function(){
    alert("redirect to /solutions/task=this/user=me, use template userSolution.html");
  }
  //
  // vm.task = {
  //   title: 'mock Task Title',
  //   mainDescription: 'this is mock Task\nThis has no real meaning\nRada Rada rada',
  //   inputDescription: 'this is mock input description, please replace this with good stuff',
  //   outputDescription: 'this is mock output description, please replace this with real shit',
  //   creatorName: 'douchey McDouchins'
  // };




  vm.showSolutionUpload = function(ev){
    console.log(vm.model);
    $mdDialog.show({

      templateUrl: '/templates/main/upload_form.html',
      fullscreen: true,
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true
    })
      .then(function(answer) {
        alert('Sprendimas išsiųstas');
        
      }, function() {
        console.log("Uzdaromas modalas");
      });
  }
});
