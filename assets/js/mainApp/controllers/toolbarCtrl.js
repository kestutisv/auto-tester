'use strict';

mainApp.controller('toolbarCtrl', function($http, $window, currentUserService) {
  var vm = this;

  vm.model = currentUserService.model;

  vm.startUp = function(){
    currentUserService.get();
  }

  vm.logout = function() {
    $http.get('/logout').then(function() {
      $window.location.reload();
    });
  }

});
