'use strict';

mainApp.controller('uploadSolutionFormCtrl', function($stateParams, $mdDialog, solutionService) {
  var vm = this;
  vm.hide = function() {
    $mdDialog.hide();
  };
  vm.cancel = function() {
    $mdDialog.cancel();
  };
  vm.answer = function(answer) {
    $mdDialog.hide(answer);
  };

  vm.submit =function(form) {
    //console.log(form);
    //console.log(form.file);
    //console.log(vm.fff);

    solutionService.send({
      task: $stateParams.id,
      code: vm.code,
      language: vm.language,
      className: vm.className
    }).then(function(res) {
      $mdDialog.hide();
    }).catch(function(err) {
      console.log(err);
    });

  }
});
