'use strict';

mainApp.controller('tasklistCtrl', function(tasksService) {
  var vm = this;

  vm.model = tasksService.model;

  vm.init = function() {
    tasksService.get()
  };

  //vm.tasklist = [
  //  {
  //    id: '111',
  //    taskName: 'mockup task Name',
  //    creatorName: 'mockup creator Name'
  //  }
  //]
});
