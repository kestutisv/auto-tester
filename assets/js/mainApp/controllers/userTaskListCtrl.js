'use strict';

mainApp.controller('userTaskListCtrl', function(currentUserService) {
  var vm = this;
  vm.userModel = currentUserService.model;

  vm.init = function(){
    currentUserService.get();
  }

  vm.tasklist = [
    {
      id: '111',
      taskName: 'mockup task Name'
    },
    {
      id: '222',
      taskName: 'mockup task Name'
    }
  ]
});
