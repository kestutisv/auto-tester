'use strict';

guestApp.controller('tasklistCtrl', function(tasksService) {
  var vm = this;
  vm.model = tasksService.model;
  vm.init = function() {
    tasksService.get();
  };
});
