'use strict';

guestApp.controller('taskCtrl', function($stateParams, taskService) {
  var vm = this;

  vm.taskId =  $stateParams.id;
  vm.model = taskService.model;

  vm.init = function(){
    taskService.get(vm.taskId);
  }
  // vm.task = {
  //   title: 'mock Task Title',
  //   mainDescription: 'this is mock Task\nThis has no real meaning\nRada Rada rada',
  //   inputDescription: 'this is mock input description, please replace this with good stuff',
  //   outputDescription: 'this is mock output description, please replace this with real shit',
  //   creatorName: 'douchey McDouchins'
  // }
  //
  // vm.examples = [
  //   { input: '1 2', output: '3'},
  //   { input: '4 5', output: '9'},
  //   { input: '123123123 321321321', output: '444444444'}
  // ]
});
