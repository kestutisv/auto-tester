'use strict';

guestApp.controller('registerCtrl', ['$http', '$state', function($http, $state) {
  var vm = this;
  var sending = false;

  vm.user = {
    username: '',
    firstName: '',
    lastName: '',
    password: '',
    pwd_repeat: '',
    email: ''
  };

  vm.send = function() {
    if(!sending) {
      sending = true;
      $http.post('/user', vm.user).then(function(res) {
        if(res.status )console.log(res);
        $state.go('login');
      }).catch(function (err) {
        if(err.status === 400) alert("Vartotojo vardas arba el.paštas jau naudojamas");
      }).finally(function() {
        sending = false;
      });
    }
  }

}]);
