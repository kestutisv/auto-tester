'use strict';

guestApp.controller('loginCtrl', ['$http', '$window', function($http, $window) {
  var vm = this;

  vm.user = {};
  vm.login = function(form) {
    if(form.$valid) {
      $http.post('/auth/local?next=/', vm.user).then(function () {
        $window.location.assign('/');
      }, function (err)
      {
        if(err.status === 403) alert("Neteisingas prisijungimo vardas arba slaptažodis");
      })
    }
  }
}]);
