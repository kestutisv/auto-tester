'use strict';

guestApp.controller('guestCtrl', ['$mdDialog', function($mdDialog) {
  var vm = this;

  vm.login = function($event) {
    $mdDialog.show({
      clickOutsideToClose: true,
      locals: {
      },
      bindToController: true,
      preserveScope: true,
      targetEvent: $event,
      templateUrl: '/templates/loginDialog.html',
      controller: function DialogController($mdDialog) {
        var vm = this;

        vm.login = function() {
          console.log('logged in as', vm.identifier, ', with', vm.password)
        }
      },
      controllerAs: 'vm'
    });
  };
}]);
