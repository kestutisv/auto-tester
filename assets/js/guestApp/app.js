
var guestApp = angular.module('guestApp', ['ngMaterial', 'ui.router']);

guestApp.config(function($stateProvider, $urlRouterProvider, $mdThemingProvider) {
  $urlRouterProvider.otherwise("/");
  $stateProvider
    .state('home', {
      url: "/",
      templateUrl: "templates/guest/home.html"
    })
    .state('login', {
      url: "/login",
      templateUrl: "templates/guest/login.html",
      controller: 'loginCtrl',
      controllerAs: 'lc'
    })
    .state('tasklist', {
      url: "/tasklist",
      templateUrl: "templates/guest/tasklist.html",
      controller: 'tasklistCtrl',
      controllerAs: 'tc'
    })
    .state('signin', {
      url: "/signin",
      templateUrl: "templates/guest/signin.html",
      controller: 'signinCtrl',
      controllerAs: 'sc'
    })
    .state('register', {
      url: "/register",
      templateUrl: "templates/guest/register.html",
      controller: 'registerCtrl',
      controllerAs: 'rc'
    })
    .state('task', {
      url: '/tasks/:id',
      templateUrl: '/templates/guest/task.html',
      title: 'Solve tasks',
      controller: 'taskCtrl as kc'
    });
  $mdThemingProvider
    .theme('default')
    .primaryPalette('amber')
    .accentPalette('lime')
    .warnPalette('red');
});
