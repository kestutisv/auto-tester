'use strict';

guestApp.factory('userTasksService', function($http) {
  var tasksService = {};

  tasksService.model = {
    tasks: []
  };

  tasksService.get = function() {
    return $http.get('/task').then(function(data) {
      tasksService.model.tasks = data.data.map(function(task) {
        return {
          id: task.id,
          name: task.title,
          creatorName: task.createdBy.name
        }
      });
    });
  };

  return tasksService;
});
