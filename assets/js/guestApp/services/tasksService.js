'use strict';

guestApp.factory('tasksService', function($http) {
  var tasksService = {};

  tasksService.model = {
    tasks: []
  };

  tasksService.get = function() {
    return $http.get('/task').then(function(data) {
      tasksService.model.tasks = data.data.map(function(task) {
        return {
          id: task.id,
          name: task.title,
          creatorName: task.createdBy ? task.createdBy.name:'unknown'
        }
      });
    });
  };

  return tasksService;
});
