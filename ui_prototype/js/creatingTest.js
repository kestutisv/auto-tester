angular.module('creatingTests', [])
    .controller('IOCtrl', function(){
        var IOList = this;
        IOList.exampleEntries = [];
		IOList.testEntries = [];

        IOList.addExampleEntry = function(){
            IOList.exampleEntries.push( {input:IOList.example_input, output:IOList.example_output} );
            IOList.example_input = '';
            IOList.example_output = '';
        }
		IOList.addTestEntry = function(){
            IOList.testEntries.push( {input:IOList.test_input, output:IOList.test_output} );
            IOList.test_input = '';
            IOList.test_output = '';
        }

        IOList.totalExamples = function(){
            var count = 0;
            angular.forEach(IOList.exampleEntries, function(){
                count++;
            })
            return count;
        }
		IOList.totalTests = function(){
            var count = 0;
            angular.forEach(IOList.testEntries, function(){
                count++;
            })
            return count;
        }
		
		IOList.invalid = function(){
			return (IOList.example_input === '' && IOList.example_output === '');
		}
		
		

		IOList.alert = function(){
			alert('Reikia sukurt modala kuris parodytu vaizdą kaip atrodys sukurtas testas');
		}


    })