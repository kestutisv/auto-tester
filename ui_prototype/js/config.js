myApp = angular.module('auto-tester', ['ngMaterial', 'ngRoute']);
myApp.controller('newTaskFormCtrl', function($scope){
	$scope.form = [
		taskName = '',
		taskMemoryLimit = '0',
		taskTimeLimit = '0',
		taskMemoryLimitState = 'MB',
		taskTimeLimitState = 's',
		taskMainDescription = '',
		taskInputDescription = '',
		taskOutputDescription = '',
		taskExampleInput = '',
		taskExampleOutput = ''
	];
	$scope.taskTestExamples = [];
	$scope.addExampleEntry = function(){
			$scope.taskTestExamples.push({id:$scope.taskTestExamples.length, input:$scope.form.taskExampleInput, output:$scope.form.taskExampleOutput});
			$scope.form.taskExampleInput = '';
			$scope.form.taskExampleOutput = ''; };
	$scope.totalExamples = function(){
            var count = 0;
            angular.forEach($scope.taskTestExamples, function(){
                count++;
            })
            return count;};
	$scope.removeEntry = function(remove){
		$scope.taskTestExamples.splice( $scope.taskTestExamples.indexOf(remove), 1);
	}
});

myApp.config( ['$routeProvider', function($routeProvider){
		$routeProvider
			.when('/home', {
				templateUrl: 'Other pages/home.html',
				title: 'Welcome'
			})
			.when('/newtask', {
				templateUrl: 'Other pages/ikelti uzduoti.html',
				title: 'Create task'
			})
			.when('/tasklist', {
				templateUrl: 'Other pages/uzduociu sarasas.html',
				title: 'Available tasks'
			})
			.when('/login', {
				templateUrl: 'Other pages/prisijungimo puslapis.html',
				title: 'Please log in'
			})
			.when('/signup', {
				templateUrl: 'Other pages/registracijos puslapis.html',
				title: 'Sign up'
			})
			.otherwise({
				redirectTo: '/home'
			});
	}]);
myApp.config(function($mdThemingProvider) {
	$mdThemingProvider.theme('default').dark();
	$mdThemingProvider.theme('default')
		.primaryPalette('blue')
		.warnPalette('red');
});
myApp.config(function($mdThemingProvider) {
  $mdThemingProvider.theme('altTheme')
	.primaryPalette('purple')
	.accentPalette('indigo');
});

myApp.run(['$location', '$rootScope', function($location, $rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {

        if (current.hasOwnProperty('$$route')) {

            $rootScope.title = current.$$route.title;
        }
    });
}]);

